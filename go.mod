module bitbucket.org/ilhamsuryapratama/viper_utility

go 1.16

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/spf13/viper v1.7.1
)
