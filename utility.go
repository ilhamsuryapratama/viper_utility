package utility

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
)

var (
	ShoPayHttpTimeout string
	ShoPayHttpRetry   int

	ShoPayClientID           string
	ShoPayClientSecret       string
	ShoPayBaseURL            string
	ShoPayCheckStatusURL     string
	ShoPayCreateOrderURL     string
	ShoPayRefundOrderURL     string
	ShoPayInvalidateOrderURL string
	ShoPayMerchantId         string
	ShoPayStoreId            string
	ShoPayCurrency           string
	ShoPayQRValidityPeriod   int
	ShoPayTerminalID         string

	Push2PayCreateOrder       string
	Push2PaySecretKey         string
	Push2PayClientId          string
	Push2PayMerchantExtID     string
	Push2PayStoreExtID        string
	Push2PayReturnUrl         string
	Push2PayPointOfInitiation string
	push2payCfg               *viper.Viper
	alCfg                     *viper.Viper
)

func init() {

	dir, _ := os.Getwd()
	os.Setenv("MAJO_APP_PATH", dir)
	AppPath := dir

	fmt.Println("AppPathd", dir)
	viper.AddConfigPath(AppPath + "/cfg")

	viper.SetConfigName("local")
	viper.SetConfigType("json")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}

	alCfg = viper.Sub("shopeepay")
	ShoPayHttpRetry = alCfg.GetInt("shopeepay_http_retry")
	ShoPayHttpTimeout = alCfg.GetString("shopeepay_http_timeout")
	ShoPayClientID = alCfg.GetString("shopeepay_client_id")
	ShoPayClientSecret = alCfg.GetString("shopeepay_client_secret")
	ShoPayBaseURL = alCfg.GetString("shopeepay_base_url")
	ShoPayCheckStatusURL = alCfg.GetString("shopeepay_check_status")
	ShoPayCreateOrderURL = alCfg.GetString("shopeepay_create_order")
	ShoPayRefundOrderURL = alCfg.GetString("shopeepay_refund_order")
	ShoPayInvalidateOrderURL = alCfg.GetString("shopeepay_invalidate_order")
	ShoPayMerchantId = alCfg.GetString("shopeepay_merchant_id")
	ShoPayStoreId = alCfg.GetString("shopeepay_store_id")
	ShoPayCurrency = alCfg.GetString("shopeepay_currency")
	ShoPayQRValidityPeriod = alCfg.GetInt("shopeepay_qr_validity_period")
	ShoPayTerminalID = alCfg.GetString("shopeepay_terminal_id")

	// push2pay config
	push2payCfg = viper.Sub("shopeepay.push2pay")
	Push2PayCreateOrder = push2payCfg.GetString("push2pay_create_order")
	Push2PaySecretKey = push2payCfg.GetString("push2pay_client_secret")
	Push2PayClientId = push2payCfg.GetString("push2pay_client_id")
	Push2PayMerchantExtID = push2payCfg.GetString("push2pay_merchant_id")
	Push2PayStoreExtID = push2payCfg.GetString("push2pay_store_id")
	Push2PayReturnUrl = push2payCfg.GetString("push2pay_return_url")
	Push2PayPointOfInitiation = push2payCfg.GetString("push2pay_point_of_initiation")
}

func Utility() (string, string) {
	return ShoPayClientID, Push2PayCreateOrder
}
